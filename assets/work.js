import baffle from "baffle";

export function changeTextAnimation(element, text) {
  baffle(element)
    .start()
    .text(currentText => text)
    .set({
      characters: "/▓░█▓_><",
      speed: 150
    })
    .reveal(1000);
}

export function colorChange(newColorHex) {
  const rgb = hexToRgb(newColorHex);
  const colorBrightness = (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;

  const lightFont = colorBrightness < 123 ? "#fff" : "#1d1e23";
  const darkFont = colorBrightness > 123 ? "#fff" : "#1d1e23";

  const borderOpacity = "0.15";
  const borderColor = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${borderOpacity})`;

  [
    "--header-circle",
    "--background-dark",
    "--font-highlight",
    "--font-dark"
  ].forEach(variable =>
    document.documentElement.style.setProperty(variable, newColorHex)
  );

  ["--header-border", "--background-light", "--font-light"].forEach(variable =>
    document.documentElement.style.setProperty(variable, lightFont)
  );

  document.documentElement.style.setProperty(
    "--table-border-dark",
    borderColor
  );
  document.documentElement.style.setProperty("--table-border-light", lightFont);

  localStorage.setItem("color", JSON.stringify({ color: newColorHex }));
}

export function resetColor() {
  [
    "--header-circle",
    "--header-border",
    "--background-light",
    "--background-dark",
    "--font-hightlight",
    "--font-dark",
    "--font-light",
    "--font-highlight",
    "--table-border-dark"
  ].forEach(variable => {
    document.documentElement.style.removeProperty(variable);
  });
  localStorage.removeItem("color");
}

export function getStoredColor() {
  const retrievedColor = localStorage.getItem("color");
  if (Boolean(retrievedColor)) {
    const newColor = JSON.parse(retrievedColor).color;
    colorChange(newColor);
  }
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null;
}

function trim(str) {
  return str.replace(/^\s+|\s+$/gm, "");
}

function rgbaToHex(rgba) {
  var parts = rgba.substring(rgba.indexOf("(")).split(","),
    r = parseInt(trim(parts[0].substring(1)), 10),
    g = parseInt(trim(parts[1]), 10),
    b = parseInt(trim(parts[2]), 10),
    a = parseFloat(trim(parts[3].substring(0, parts[3].length - 1))).toFixed(2);

  return (
    "#" +
    r.toString(16) +
    g.toString(16) +
    b.toString(16) +
    (a * 255).toString(16).substring(0, 2)
  );
}
