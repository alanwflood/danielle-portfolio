import Prismic from "prismic-javascript";

export const state = () => ({
  totalPosts: 0,
  blogEntries: [],
  blogPage: 1
});

export const mutations = {
  SET_BLOG_ENTRIES: (state, blogEntries) => (state.blogEntries = blogEntries),
  SET_BLOG_PAGE: (state, blogPage) => (state.page = blogPage),
  SET_TOTAL_BLOG_PAGES: (state, totalPosts) => (state.totalPosts = totalPosts)
};

export const actions = {
  async GET_BLOG_ENTRIES({ commit }, pageNumber, pageSize = 12) {
    commit("SET_BLOG_PAGE", pageNumber);
    const blogEntries = await Prismic.getApi(process.env.PRISMIC_URL)
      .then(api =>
        api.query(Prismic.Predicates.at("document.type", "project_365"), {
          pageSize,
          page: pageNumber,
          orderings: "[my.project_365.order desc]"
        })
      )
      .then(response => {
        commit("SET_TOTAL_BLOG_PAGES", response.total_results_size);
        const prismicData = response.results.map(entry =>
          Object.assign(entry.data, { id: entry.id, uid: entry.uid })
        );
        return prismicData.map(entry =>
          Object.assign(
            ...Object.keys(entry)
              .filter(key => ["img", "id", "uid"].includes(key))
              .map(key => ({ [key]: entry[key] })),
            { img_cap: entry.img_cap[0].text },
            entry.link.hasOwnProperty("url") ? { url: entry.link.url } : {}
          )
        );
      })
      .catch(err =>
        console.log("Could not get blog entries from Prismic: ", err)
      );
    commit("SET_BLOG_ENTRIES", blogEntries);
  }
};
