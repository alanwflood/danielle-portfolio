import Prismic from "prismic-javascript";

export const state = () => ({
  info_data: {}
});

export const mutations = {
  SET_INFO: (state, info) => (state.info_data = info)
};

export const actions = {
  async GET_INFO({ commit }) {
    const info = await Prismic.getApi(process.env.PRISMIC_URL)
      .then(api =>
        api.query(Prismic.Predicates.at("document.type", "info_page"))
      )
      .then(response => {
        const prismicData = response.results[0].data;
        return Object.assign(
          {},
          prismicData,
          {
            interview_links: prismicData.interview_links.map(link => ({
              url: link.interview_link.url,
              text: link.interview_link_title[0].text
            }))
          },
          {
            misc_images: prismicData.misc_images.map(image => image.misc_image)
          }
        );
      })
      .catch(err => console.log("Could not get info page from Prismic: ", err));
    commit("SET_INFO", info);
  }
};
