import Prismic from "prismic-javascript";

export const state = () => ({
  workPortfolio: []
});

export const mutations = {
  SET_WORK_PORTFOLIO(state, workPortfolio) {
    state.workPortfolio = workPortfolio;
  }
};

export const actions = {
  async GET_WORK_PORTFOLIO({ commit }) {
    const workPortfolio = await Prismic.getApi(process.env.PRISMIC_URL)
      .then(api =>
        api.query(Prismic.Predicates.at("document.type", "work_detail"))
      )
      .then(response => {
        const prismicData = response.results.map(entry =>
          Object.assign(entry.data, { id: entry.id, uid: entry.uid })
        );
        return prismicData.map(entry =>
          Object.assign(
            ...Object.keys(entry)
              .filter(key =>
                ["date", "client", "actions", "details"].includes(key)
              )
              .map(key => ({ [key]: entry[key][0].text })),
            ...Object.keys(entry)
              .filter(
                key => !["date", "client", "actions", "details"].includes(key)
              )
              .map(key => ({ [key]: entry[key] }))
          )
        );
      })
      .catch(err =>
        console.log("Could not get work details from Prismic: ", err)
      );

    commit("SET_WORK_PORTFOLIO", workPortfolio);
  }
};
