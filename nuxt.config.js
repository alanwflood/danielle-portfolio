module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "DM_CREATIVE_LAB",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Danielle Morgan&apos;s personal website"
      },
      {
        name: "theme-color",
        content: "#1d1e23"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "//brick.a.ssl.fastly.net/Roboto+Mono:300,400"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicon-32x32.png"
      },
      {
        rel: "apple-touch-icon",
        type: "image/png",
        size: "152x152",
        href: "/apple-touch-icon"
      },
      {
        rel: "manifest",
        href: "/site.webmanifest"
      }
    ]
  },
  modules: ["@nuxtjs/dotenv"],
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },
  css: ["~/assets/variables.css", "~/assets/tables.css", "~/assets/main.css"],
  /*
  ** Build configuration
  */
  build: {
    postcss: [
      require('postcss-cssnext')({
        features: {
          customProperties: false
        }
      })
    ],
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  }
};
